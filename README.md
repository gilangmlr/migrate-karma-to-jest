# migrate-karma-to-jest

This is the home for a script that migrates Karma tests in gitlab-ce/ee to Jest.

<img src="karma-to-jest.png">

### What do I need to run this?

- **Node 12.4** - Otherwise you might see a warning about `the fs.promises API is experimental`.
- **Yarn**

### How do I run this thing?

It's recommended to install this package globally. If you have checked out this repository, you can do this by running: 

```
yarn global add file:$PWD
```

Once installed, the command is:

```
migrate-karma-to-jest <input_dir> <output_dir>
```

After running the script, you'll want to run prettier and eslint to clean up the output files.

### How do I test this thing?

Simply run the integration test script. If there's no `diff` output, then the test has passed :thumbsup:

```
./integration_spec/run.sh
```
