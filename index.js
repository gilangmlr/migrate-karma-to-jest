#!/usr/bin/env node
const pjson = require('./package.json');
const program = require('commander');
const main = require('./src/main');

const onError = err => {
  const { message } = err;
  console.log(`ERROR: ${message}`);
  process.exitCode = 1;

  throw err;
};

program
  .version(pjson.version, '-v --version')
  .option('-d, --root-dir <path>', 'Working directory', process.cwd())
  .option('-w, --webpack-config <path>', 'Webpack config')
  .arguments('<input> <output>')
  .action((input, output, options) => {
    main(input, output, options).catch(onError);
  });

program.parse(process.argv);
