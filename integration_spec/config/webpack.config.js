const path = require('path');

module.exports = {
  resolve: {
    alias: {
      '~': path.dirname(__dirname),
    }
  },
};
