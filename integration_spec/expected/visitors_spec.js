import mock from 'helpers/mock';
import superMock from 'helpers/super_mock';
import sut from '~/lorem/ipsum';
describe('lorem ipsum', () => {
  beforeEach(() => {
    jest.spyOn(sut, 'lorem').mockReturnValue();
    jest.spyOn(sut, 'ipsum').mockReturnValue(3);
  });
  it('lorem', () => {
    expect(sut.items).toContain({
      name: 'dolar sit amit'
    });
  });
});