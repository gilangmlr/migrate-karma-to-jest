import mock from 'spec/mock';
import superMock from 'spec/helpers/super_mock';
import sut from '~/lorem/ipsum';

describe('lorem ipsum', () => {
  beforeEach(() => {
    spyOn(sut, 'lorem');
    spyOn(sut, 'ipsum').and.returnValue(3);
  });

  it('lorem', () => {
    expect(sut.items).toContain({ name: 'dolar sit amit' });
  });
});
