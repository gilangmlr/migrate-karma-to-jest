const visitors = require('./visitors');
const { parse, traverse, generate } = require('./parser');

function migrate(content, filePath) {
  const visitorOptions = { filePath };
  const ast = parse(content, { sourceType: 'module' });

  visitors.forEach(visitor => {
    traverse(ast, visitor(visitorOptions));
  });

  const newCode = generate(ast).code;

  return newCode;
}

module.exports = migrate;
