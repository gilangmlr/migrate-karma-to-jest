const parser = require('@babel/parser');
const traverse = require('@babel/traverse').default;
const generate = require('@babel/generator').default;
const fs = require('fs');

function parse(content, options = {}) {
  return parser.parse(content, { sourceType: 'module', ...options });
}

function parseFile(path) {
  const content = fs.readFileSync(path, { encoding: 'utf-8' });

  return parse(content);
}

function traverseFile(path, visitor) {
  traverse(parseFile(path), visitor);
}

module.exports = {
  generate,
  parse,
  parseFile,
  traverse,
  traverseFile,
};
