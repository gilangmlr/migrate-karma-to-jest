const fs = require('fs').promises;
const path = require('path');
const _ = require('lodash');

function joinDir(dir, file) {
  return Object.assign(file, { name: path.join(dir, file.name) });
}

function readdirRecursive(dir, options = {}) {
  return fs.readdir(dir, { ...options, withFileTypes: true })
    .then(children => children.map(x => joinDir(dir, x)))
    .then(children => {
      const { directories = [], files = [] } = _.groupBy(children, x => x.isDirectory() ? 'directories' : 'files');

      return Promise.all(directories.map(x => readdirRecursive(x.name, options)))
        .then(_.flattenDeep)
        .then(others => {
          return files.concat(others);
        });
    });
}

module.exports = readdirRecursive;
