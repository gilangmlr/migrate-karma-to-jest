const t = require('@babel/types');
const { isRHSOfMemberExpression, createVisitor } = require('../utils/ast');

function jestifyClocks() {
  return createVisitor('Identifier', {
    clock(path) {
      if(!isRHSOfMemberExpression(path, 'jasmine')) {
        return;
      }

      const statement = path.getStatementParent();

      const { callee } = statement.node.expression;

      if (t.isMemberExpression(callee) && t.isIdentifier(callee.property, { name: 'tick' })) {
        callee.object = t.identifier('jest');
        callee.property = t.identifier('advanceTimersByTime');
      } else {
        statement.remove();
      }
    }
  });
};

module.exports = jestifyClocks;
