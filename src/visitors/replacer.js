const { createVisitor } = require('../utils/ast');

function replacer(from, to) {
  return () => createVisitor('Identifier', {
    [from](path) {
      path.node.name = to;
    },
  });
}

module.exports = replacer;
