const t = require('@babel/types');
const { dirname } = require('path');
const {
  createVisitor,
  createStringLiteral,
  getRoot,
  isRHSOfMemberExpression,
  query,
  queryFile,
} = require('../utils/ast');
const { resolve } = require('../utils/resolver');

const buildJestMock = mockPath => t.expressionStatement(t.callExpression(
  t.memberExpression(t.identifier('jest'), t.identifier('mock')),
  [createStringLiteral(mockPath)],
));

const buildImport = (importName, path) => t.importDeclaration(
  [t.importDefaultSpecifier(t.identifier(importName))],
  createStringLiteral(path),
);

function ensureMemberExpression(path, name) {
  if (path.parentPath.isMemberExpression()) {
    return;
  }

  const statement = path.getStatementParent();
  const obj = t.memberExpression(path.node, t.identifier(name));
  statement.node.expression = t.callExpression(obj, []);
}

function hasImportName({ node }, name) {
  return node.specifiers.find(({ local }) => local && local.name === name);
}

function jestifySpies({ filePath }) {
  return createVisitor('Identifier', {
    spyOn(path) {
      if(isRHSOfMemberExpression(path, 'jest')) {
        return;
      }

      path.replaceWithSourceString('jest.spyOn');
      ensureMemberExpression(path.parentPath, 'mockReturnValue');
    },
    returnValue(path) {
      if(!isRHSOfMemberExpression(path, 'and')) {
        return;
      }

      const expr = path.parent;
      expr.object = expr.object.object;
      expr.property = t.identifier('mockReturnValue');
    },
    // Example: spyOnDependency(IMPORT_NAME, 'MOCK_NAME')
    spyOnDependency(path) {
      const root = getRoot(path);
      const importName = path.parent.arguments[0].name;
      const mockName = path.parent.arguments[1].value;

      // setup: get the file imported as importName
      const [importDec] = query(root, 'ImportDeclaration', x => hasImportName(x, importName));
      const importPath = resolve(dirname(filePath), importDec.node.source.value);

      // setup: get that imported file's import path for mockName
      const [mockImportDec] = queryFile(importPath, 'ImportDeclaration', x => hasImportName(x, mockName));
      const mockPath = mockImportDec.node.source.value;

      // migrate: add jest.mock call for mock path
      const lastImport = root.get('body').filter(x => t.isImportDeclaration(x)).pop();
      lastImport.insertAfter(buildJestMock(mockPath));

      // migrate: add import for mockPath if `spyOnDependency` was assigned a value.
      const pathExpression = path.find(x => t.isAssignmentExpression(x) || t.isVariableDeclaration(x));
      if (pathExpression) {
        const isAssignment = t.isAssignmentExpression(pathExpression);
        const { name } = isAssignment ? pathExpression.node.left : pathExpression.node.id;
        lastImport.insertAfter(buildImport(name, mockPath));

        // migrate: remove the variable declaration
        if (isAssignment) {
          path.scope.getBinding(name).path.getStatementParent().remove();
        }
      }

      // migrate: remove spyOnDependency call.
      path.getStatementParent().remove();
    },
  });
}

module.exports = jestifySpies;
